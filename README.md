# ParTCP Server

The ParTCP (pronounced "participy") server is a platform for conducting
democratic decision making processes.


## System requirements

- Linux
- OpenSSL
- Apache2 with headers module
- PHP 7 with OpenSSL support and sodium extension
- Git


## Installation

The following instructions assume that the root directory of your web server
is `/var/www/my-partcp.org`, the address is `my-partcp.org`, and the web
server is run by the user `www-data`.

1. **Clone project to your web server (as root)**

	````sh
	cd /var/www
	sudo git clone https://gitlab.com/diebasis-partei/partcp/partcp-server.git
	sudo mv partcp-server my-partcp.org
	````

2. **Make all files and directories accessible for all users**

	````sh
	cd my-partcp.org
	sudo find . -type f -exec chmod 644 {} \;
	sudo find . -type d -exec chmod 755 {} \;
	sudo chmod +x daemon/partcpd
	````

3. **Create directory for temporary files**

	````sh
	sudo mkdir tmp
	sudo chown root:www-data tmp
	sudo chmod 770 tmp
	````

4. **Adapt Apache configuration for ParTCP root directory**

	````apache
	<Directory "/var/www/my-partcp.org">
		# Make resources accessible regardless of client domain
		Header set Access-Control-Allow-Origin "*"
		Header set Access-Control-Allow-Methods "GET, POST, OPTIONS"
		Header set Access-Control-Allow-Headers "Origin, Content-Type, Cookie, Accept, Authorization, Access-Control-Allow-Origin, User-Agent"
		
		# Enable auto indexing and other options
		Options Indexes FollowSymLinks MultiViews

		# Auto index even if index file is present, and show hidden items
		DirectoryIndex disabled
		IndexIgnoreReset On

		# Have browsers handle YAML and Markdown files like plain text files
		AddType text/plain yaml md
		AddDefaultCharset utf-8

		# Let all POST requests be handled by process.php script
		RewriteEngine on
		RewriteCond %{THE_REQUEST} ^POST [NC]
		RewriteRule .* process.php
	</Directory>
	<Directory ~ "^/var/www/my-partcp.org/.+">
		# Do not execute PHP scripts in subdirectories, deliver them as plain text instead
		php_flag engine off
		AddType text/plain php
		AddDefaultCharset utf-8
	</Directory>
	````

5. **Optional: Install a fancy auto index module**

	For example: https://github.com/Tymek/fancyindex

6. **Install ParTCP daemon process**

	See [Readme file in daemon subdirectory](daemon/README.md).

7. **Configure ParTCP**

	````sh
	sudo cp config-example.yaml config.yaml
	sudo nano config.yaml
	# adapt configuration parameters to your needs
	````
8. **Send test message**

	Check if the server works properly by sending a "ping" message:

	````sh
	curl -d "Message-Type: ping" my-partcp.org
	````
	
	You should receive a response like this:

	````
	Signature: WHM08/cytInHm2h4z64J1pUjNg2U88B1q1ovZA5ETxSkJYhJQuKjn2RoA9X6k5Y2HCOTrrVcfRpk6/agk8trAg==
	From: partcp.virthos.net
	Date: 2021-03-30T09:06:56+02:00
	Message-Type: receipt
	Original-Message: 'Message-Type: ping'
	Verification-Result: missing signature, nothing to verify
	Public-Key: >
	  8BbYgAcG4yPfKKhh64+uo/AxV0NkcxhIAmnnHq1aAosBJQqBJk1Fy6Hj3g6vsZxOv4W/gN4NwzLTIGerDMNxHg==
	Elapsed-Time: 0.585 ms
	````

