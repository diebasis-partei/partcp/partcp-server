# Daemon for ParTCP server

This daemon is responsible for performing tasks that need root privileges (and
some other tasks just for convenience):

- signing messages
- verifying signatures
- encrypting strings
- decrypting strings
- creating directories
- writing (new) files


## Installation

1. Create `partcp` group and add web server user (e.g. www-data):

	````sh
	sudo addgroup partcp
	sudo usermod -a -G partcp www-data
	````

2. Create neccessary directories, set group and privileges:

	````sh
	sudo mkdir -p /etc/partcp/tmp
	sudo chgrp -R partcp /etc/partcp
	sudo chmod 750 /etc/partcp /etc/partcp/tmp
	````

3. Adapt partcpd.service file and install it. 

	````sh
	sudo cp daemon/partcpd.service /etc/systemd/system
	sudo nano /etc/systemd/system/partcpd.service
	# set path of partcpd file in `ExecStart` and save changes
	````

4. Start service and check status:

	````sh
	sudo systemctl start partcpd
	sudo systemctl status partcpd
	````

5. Restart web server:

	````sh
	sudo systemctl restart apache2
	````


## Running multiple daemon instances

In order to evenly distribute the computational load when multiple processors
are available, it is possible to run multiple instances of the daemon process
simultaneously. To achieve this you have to provide multiple systemd service
files with slightly adapted `ExecStart` parameter, for example:

````
# file partcpd1.service
...
ExecStart=/bin/sh -c '/var/www/daemon/partcpd -s1 2>&1 > /var/log/partcpd.log'
...
````

````
# file partcpd2.service
...
ExecStart=/bin/sh -c '/var/www/daemon/partcpd -s2 2>&1 > /var/log/partcpd.log'
...
````

Install and start each service (see above, step 3 and 4).

Tell the ParTCP server to make use of both daemons by specifying the 
`Server-Sockets` parameter:

````
Server-Sockets: [ '1', '2' ]
````

Now, with every invocation the server will randomly choose one of the daemon 
processes and thus evenly distribute the computational load between them.

