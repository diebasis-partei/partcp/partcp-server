<?php

class Participants {
	
	public $dataDir;
	public $baseDir;
	public $options;


	public function __construct( $dataDir, $options = [] ){
		$this->dataDir = $dataDir;
		$this->options = $options;
	}


	public function set_base_dir( $baseDir ){
		$this->baseDir = rtrim( $baseDir, '/' );
	}


	public function get_dir( $id, $absolute = FALSE ){
		if ( ! empty( $this->options['subdir_depth'] ) ){
			$parts = str_split( md5( $id ), $this->options['subdir_name_length'] ?? 2 );
			$subdirs = implode( '/', array_slice( $parts, 0, $this->options['subdir_depth'] ) );
			$id = "{$subdirs}/{$id}";
		}
		$relativeDir = ( $this->baseDir ? "{$this->baseDir}/" : '' ) . "participants/{$id}";
		return ( $absolute ? $this->dataDir . '/' : '' ) . $relativeDir;
	}


	public function get_data( $id ){
		$dir = $this->get_dir( $id, TRUE );
		if ( ! is_dir( $dir ) ){
			return FALSE;
		}
		$data = array ( 'id' => $id );
		$listing = scandir( $dir );
		foreach ( $listing as $fileName ){
			if ( $fileName[0] == '.' ){
				continue;
			}
			$type = substr( $fileName, 16 );
			if ( in_array( $type, [ 'registration', 'registration-note', 'key-submission', 'key-renewal-permission' ] ) ){
				$receipt = spyc_load_file( "{$dir}/{$fileName}" );
				if ( empty( $receipt['Original-Message'] ) ){
					continue;
				}
				$message = spyc_load( $receipt['Original-Message'] );
				if ( empty( $message['Message-Type'] ) ){
					continue;
				}
				if ( $type == 'registration' ){
					if ( empty( $message['Credential'] )
					){
						continue;
					}
					$data['credential'] = $message['Credential'];
					$data['name'] = $message['Name'] ?? '';
					$data['division'] = $message['Division'] ?? '';
				}
				elseif ( $type == 'registration-note' ){
					if ( empty( $message['Credential'] )
					){
						continue;
					}
					$data['credential'] = $message['Credential'];
					$data['name'] = $message['Name'] ?? '';
					$data['division'] = $message['Division'] ?? '';
				}
				elseif ( $type == 'key-submission' ){
					if ( empty( $message['Public-Key'] ) ){
						continue;
					}
					$data['public_key'] = $message['Public-Key'];
					unset( $data['credential'] );
				}
				elseif ( $type == 'key-renewal-permission' ){
					if ( empty( $message['Participant-Id'] ) 
						|| empty( $message['Credential'] )
					){
						continue;
					}
					$data['credential'] = $message['Credential'];
				}
				elseif ( $type == 'lot-invalidation' ){
					return FALSE;
				}
			}
		}
		return $data;
	}
}

// end of file models/participant.class.php

