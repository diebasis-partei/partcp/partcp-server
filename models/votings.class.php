<?php

class Votings {
	
	public $dataDir;
	public $baseDir;


	public function __construct( $dataDir ){
		$this->dataDir = $dataDir;
	}


	public function set_base_dir( $baseDir ){
		$this->baseDir = rtrim( $baseDir, '/' );
	}


	public function get_dir( $id, $absolute = FALSE ){
		$relativeDir = ( $this->baseDir ? "{$this->baseDir}/" : '' ) . "votings/{$id}";
		return ( $absolute ? $this->dataDir . '/' : '' ) . $relativeDir;
	}


	public function get_list( $status = NULL ){
		$dir = ( $this->baseDir ? "{$this->baseDir}/" : '' ) . 'votings';
		if ( ! is_dir( "{$this->dataDir}/{$dir}" ) ){
			return array ();
		}
		$dirList = scandir( "{$this->dataDir}/{$dir}" );
		$votings = array ();
		foreach ( $dirList as $item ){
			if ( $item[0] == '.' ){
				continue;
			}
			$data = $this->get_data( $item );
			if ( empty( $status ) || $status == 'all'
				|| $data['status'] == $status )
			{
				$votings[] = $this->get_data( $item );
			}
		}
		return $votings;
	}

	public function get_data( $id, $forListing = FALSE ){
		$dir = $this->get_dir( $id, TRUE );
		if ( ! is_dir( $dir ) || ! file_exists( "{$dir}/voting-definition" ) ){
			return FALSE;
		}
		$data = array ( 'id' => $id, 'status' => 'idle' );
		$receipt = spyc_load_file( "{$dir}/voting-definition" );
		$message = spyc_load( $receipt['Original-Message'] );
		$data['name'] = $message['Voting-Name'] ?? '';
		$data['title'] = $message['Voting-Title'] ?? '';
		if ( ! $forListing ){
			$data['division'] = $message['Voting-Division'] ?? '';
			$data['description'] = $message['Voting-Description'] ?? '';
			$data['type'] = $message['Voting-Type'] ?? '';
			$data['options'] = $message['Voting-Options'] ?? 'x';
			if ( file_exists( "{$dir}/voting-start-declaration" ) ){
				if ( file_exists( "{$dir}/voting-end-declaration" ) ){
					if ( file_exists( "{$dir}/vote-count" ) ){
						$data['status'] = 'finished';
						$result = spyc_load_file( "{$dir}/vote-count" );
						$data['result'] = $result['Voting-Result'];
					}
					else {
						$data['status'] = 'closed';
					}
				}
				else {
					$data['status'] = 'open';
				}
			}
		}
		return $data;
	}


	public function count_votes( $votingId ){
		$ballotsDir = $this->get_dir( $votingId, TRUE ) . '/ballots';
		if ( ! is_dir( $ballotsDir ) ){
			return array (
				'participants' => 0,
				'invalid' => 0,
				'options' => []
			);
		}
		$dirList = scandir( $ballotsDir );
		$participantCount = 0;
		$invalidVoteCount = 0;
		$result = array ();
		foreach ( $dirList as $participant ){
			if ( $participant[0] == '.' ){
				continue;
			}
			$participantCount++;
			$votes = $this->get_participant_votes( $ballotsDir, $participant );
			if ( ! $votes ){
				$invalidVoteCount++;
				continue;
			}
			foreach ( $votes as $vote ){
				$idxOption = 'option:' . $vote['id'];
				$idxVote = 'vote:' . $vote['vote'];
				if ( empty( $result[ $idxOption ][ $idxVote ] ) ){
					$result[ $idxOption ][ $idxVote ] = 1;
				}
				else {
					$result[ $idxOption ][ $idxVote ]++;
				}
			}
		}
		return array (
			'participants' => $participantCount,
			'invalid' => $invalidVoteCount,
			'options' => $result
		);
	}


	private function get_participant_votes( $ballotsDir, $ptcpId ){
		$dir = "{$ballotsDir}/{$ptcpId}";
		if ( ! is_dir( $dir ) ){
			return FALSE;
		}
		$listing = scandir( $dir, SCANDIR_SORT_DESCENDING );
		foreach ( $listing as $fileName ){
			if ( $fileName[0] == '.' ){
				continue;
			}
			if ( substr( $fileName, 16 ) != 'ballot' ){
				continue;
			}
			$receipt = spyc_load_file( "{$dir}/{$fileName}" );
			if ( empty( $receipt['Original-Message'] ) ){
				continue;
			}
			$ballot = spyc_load( $receipt['Original-Message'] );
			if ( empty( $ballot['Votes'] ) ){
				continue;
			}
			return $ballot['Votes'];
		}
		return FALSE;
	}
}

// end of file models/votings.class.php

