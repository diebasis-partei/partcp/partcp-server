<?php

class Lots {

	public $dataDir;
	public $baseDir;
	public $options;


	public function __construct( $dataDir, $options = [] ){
		$this->dataDir = $dataDir;
		$this->options = $options;
	}


	public function set_base_dir( $baseDir ){
		$this->baseDir = rtrim( $baseDir, '/' );
	}


	public function get_dir( $id, $absolute = FALSE ){
		if ( ! empty( $this->options['subdir_depth'] ) ){
			$parts = str_split( md5( $id ), $this->options['subdir_name_length'] ?? 2 );
			$subdirs = implode( '/', array_slice( $parts, 0, $this->options['subdir_depth'] ) );
			$id = "{$subdirs}/{$id}";
		}
		$relativeDir = ( $this->baseDir ? "{$this->baseDir}/" : '' ) . "lots/{$id}";
		return ( $absolute ? $this->dataDir . '/' : '' ) . $relativeDir;
	}

}

// end of file models/lots.class.php

