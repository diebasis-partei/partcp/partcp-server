<?php

class Events {
	
	public $dataDir;
	public $options;


	public function __construct( $dataDir, $options = [] ){
		$this->dataDir = $dataDir;
		$this->options = $options;
	}


	public function get_dir( $id, $absolute = FALSE ){
		$dir = substr( $id, 0, 8 );
		return ( $absolute ? $this->dataDir . '/' : '' ) . "events/{$dir}/{$id}";
	}


	public function get_list(){
		if ( ! file_exists( "{$this->dataDir}/events" ) ){
			return array ();
		}
		$dayList = scandir( "{$this->dataDir}/events" );
		if ( ! $dayList ){
			return array ();
		}
		$events = array ();
		foreach ( $dayList as $day ){
			if ( $day[0] == '.' ){
				continue;
			}
			$eventList = scandir( "{$this->dataDir}/events/{$day}" );
			foreach ( $eventList as $eventId ){
				if ( $eventId[0] != '.' ){
					$events[] = $this->get_data( $eventId, TRUE );
				}
			}
		}
		return $events;
	}


	public function get_data( $id ){
		$dir = $this->get_dir( $id, TRUE );
		if ( ! is_dir( $dir ) ){
			return FALSE;
		}
		$data = array ( 'id' => $id );
		$listing = scandir( $dir );
		foreach ( $listing as $fileName ){
			if ( $fileName[0] == '.' ){
				continue;
			}
			$type = substr( $fileName, 16 );
			if ( in_array( $type, [ 'event-definition' ] ) ){
				$receipt = spyc_load_file( "{$dir}/{$fileName}" );
				if ( empty( $receipt['Original-Message'] ) ){
					continue;
				}
				$message = spyc_load( $receipt['Original-Message'] );
				if ( empty( $message['Message-Type'] ) ){
					continue;
				}
				if ( $type == 'event-definition' ){
					$data['name'] = $message['Event-Name'] ?? '';
					$data['date'] = $message['Event-Date'] ?? '';
					$data['paper_lots'] = $message['Event-Paper-Lots'] ?? FALSE;
				}
			}
		}

		if ( empty( $data['name'] ) ){
			$data['name'] = substr( $id, 9 );
		}
		if ( empty( $data['date'] ) ){
			$data['date'] = substr( $id, 0, 4 ) . '-' . substr( $id, 4, 2 ) . '-' . substr( $id, 6, 2 );
		}
		$listing = file_exists( "{$dir}/lots" ) ? scandir( "{$dir}/lots" ) : NULL;
		$data['lot_codes'] = $listing && count( $listing ) > 2;
		return $data;
	}
}

// end of file models/events.class.php

