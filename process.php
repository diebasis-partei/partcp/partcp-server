<?php

### Process incoming messages ###

function http_error( $statusCode, $body = '' ){
	header( $_SERVER['SERVER_PROTOCOL'] . ' ' . $statusCode );
	die( $body );
}


function send_error_message( $errorCode, $errorMessage = '' ){
	global $Message;
	$Message->set_header( 'Message-Type', 'error-message' );
	$Message->set_header( 'Error-Code', $errorCode );
	$Message->set_body( $errorMessage );
}


function str2filename( $str ){
	$str = strtolower( $str );
	$str = preg_replace( '/\s+/', '-', $str );
	$str = str_replace( ['ä','ö','ü','ß'], ['ae','oe','ue','ss'], $str );
	$str = html_entity_decode( $str, ENT_QUOTES, 'utf-8' );
	$str = htmlentities( $str, ENT_QUOTES, 'utf-8' );
	$str = preg_replace( '/(&)([a-z])([a-z]+;)/i', '$2', $str );
	$str = preg_replace( '/[^a-zA-Z0-9\-\._]/', '', $str );
	return $str;
}


function get_sender_key( $forCrypto = FALSE ){
	global $Config, $DataDir, $Message, $Sender, $senderIsRegistrar;
	static $keyCrypto, $keySign;

	if ( empty( $Message['From'] ) ){
		return FALSE;
	}
	list ( $senderId, $server ) = explode( '@', $Message['From'] ) + [ '', '' ];
	if ( ! empty( $Config['Master-Server'] ) 
		|| ( $server && $server != $Config['Server-Name'] )
	){
		// retrieve key from master server
		// TODO: Implement master-slave mechanism
	}
	elseif ( ! empty( $Config['Registrars'] ) 
		&& in_array( $Message['From'], array_keys( $Config['Registrars'] ) )
	){
		// get key of registrar
		$key = base64_decode( $Config['Registrars'][ $Message['From'] ] );
		$senderIsRegistrar = TRUE;
	}
	else {
		// retrieve key from local storage
		$ptcpModel = new Participants( $DataDir, $Config['Participants-Model'] );
		if ( strpos( $senderId, '+' ) ){
			list( $eventId, $senderId ) = explode( '+', $senderId );
		}
		elseif ( ! empty( $Message['Event-Id'] ) ){
			$eventId = $Message['Event-Id'];
		}
		if ( isset( $eventId ) ){
			$events = new Events( $DataDir );
			$ptcpModel->set_base_dir( $events->get_dir( $eventId ) );
		}
		$Sender = $ptcpModel->get_data( $senderId );
		$key = empty( $Sender['public_key'] ) ? NULL : base64_decode( $Sender['public_key'] );
	}
	if ( empty( $key ) && $forCrypto && ! empty( $Message['Public-Key'] ) ){
		$key = base64_decode( $Message['Public-Key'] );
	}
	if ( empty( $key ) ){
		return FALSE; // sender unknown
	}
	if ( $forCrypto ){
		$key = substr( $key, SODIUM_CRYPTO_SIGN_PUBLICKEYBYTES );
	}
	else {
		$key = substr( $key, 0, SODIUM_CRYPTO_SIGN_PUBLICKEYBYTES );
	}
	return $key;
}


function generate_signature( $message ){
	return Partcp::sign( $message );
}


function verify_signature( $useEmbeddedKey = FALSE ){
	global $Message, $RawMessage;
	if ( empty( $Message['Signature'] ) ){
		return FALSE;
	}
	if ( $useEmbeddedKey && ! empty( $Message['Public-Key'] ) ){
		$key = base64_decode( $Message['Public-Key'] );
		$key = substr( $key, 0, SODIUM_CRYPTO_SIGN_PUBLICKEYBYTES );
	}
	else {
		$key = get_sender_key();
	}
	if ( ! $key ){
		return FALSE;
	}
	$signature = base64_decode( $Message['Signature'] );
	$unsignedMessage = substr( $RawMessage, strpos( $RawMessage, "\n" ) + 1 );
	$result = Partcp::verify( $signature, $unsignedMessage, $key );
	return $result;
}


function generate_hash( $message ){
	return Partcp::hash( $message );
}


function encrypt_value( $value ){
	$key = get_sender_key( TRUE );
	if ( ! $key ){
		return FALSE;
	}
	return Partcp::encrypt( $value, $key );
}


function decrypt_value( $value ){
	$key = get_sender_key( TRUE );
	if ( ! $key ){
		return FALSE;
	}
	return Partcp::decrypt( $value, $key );
}


function encrypt_local( $value ){
	return Partcp::encrypt( $value );
}


function decrypt_local( $value ){
	return Partcp::decrypt( $value );
}


function make_directory( $path, $zeroTime = FALSE ){
	global $Config;
	if ( $path[0] != '/' ){
		$path = rtrim( $Config['Data-Directory'], '/' ) . '/' . ltrim( $path, '/' );
	}
	return Partcp::mkdir( $path, $zeroTime );
}


function put_file( $path, $value, $zeroTime = FALSE ){
	global $Config;
	if ( $path[0] != '/' ){
		$path = rtrim( $Config['Data-Directory'], '/' ) . '/' . ltrim( $path, '/' );
	}
	$result = Partcp::put( $path, $value, $zeroTime );
	return $result == 'OK';
}


function shutdown(){
	Partcp::shutdown();
}


### Main Script ###

$Timestamp = time();
$startTime = microtime( TRUE );
$BaseDir = __DIR__;
error_reporting( E_ALL );
ini_set( 'display_errors', 1 );
register_shutdown_function('shutdown');

set_include_path( get_include_path() . PATH_SEPARATOR . $BaseDir . '/models' );
spl_autoload_extensions('.class.php');
spl_autoload_register();

require_once 'lib/spyc.php';
$Config = spyc_load_file('config.yaml');
$DataDir = empty( $Config['Data-Directory'] ) ? __DIR__ . '/data' : $Config['Data-Directory'];
$TempDir = empty( $Config['Temp-Directory'] ) ? __DIR__ . '/tmp' : $Config['Temp-Directory'];

// If there is nothing to process, show contents of this script file

if ( empty( $_POST['message'] ) ){
	$_POST['message'] = file_get_contents('php://input');
	if ( empty( $_POST['message'] ) ){
		header('Content-Type: text/plain');
		readfile( __FILE__ );
		exit;
	}
}


header('Content-Type: text/plain');
require_once 'lib/partcp.class.php';
$RawMessage = str_replace( ["\r\n", "\r"], "\n", trim( $_POST['message'] ) );
$Message = spyc_load( $RawMessage );
$senderIsRegistrar = FALSE;

$socketFile = tempnam( $TempDir, 'client.socket.' );
unlink( $socketFile ); // make file name availabe for socket usage
if ( ! Partcp::init( $socketFile ) ){
	http_error( '500 Internal Server Error', "Could not initialize daemon client with socket {$socketFile}" );
}
if ( ! empty( $Config['Server-Sockets'] ) ){
	foreach ( $Config['Server-Sockets'] as $sock ){
		$sock = trim( $sock );
		$serverSockets[] = "/etc/partcp/socket{$sock}";
	}
}
else {
	$serverSockets = array ( '/etc/partcp/socket' );
}
Partcp::$serverSocket = $serverSockets[ array_rand( $serverSockets ) ];



// Check if message contains a valid type

if ( empty( $Message['Message-Type'] ) ){
	http_error( '400 Bad Request', 'Message-Type header is missing' );
}

$messageType = $Message['Message-Type'];
$messageHandler = __DIR__ . "/handlers/{$messageType}.php";

if ( ! file_exists( $messageHandler ) ){
	http_error( '501 Not Implemented', "Unknown message type '{$messageType}'" );
}

// Decrypt encrypted fields

foreach ( $Message as $key => $value ){
	if ( substr( $key, -1 ) == '~' ){
		$decrypted = decrypt_value( $value );
		if ( $decrypted !== FALSE ){
			$Message[ substr( $key, 0, -1 ) ] = $decrypted;
		}
	}
}

// Prepare receipt

$Receipt['From'] = $_SERVER['SERVER_NAME'];
if ( ! empty( $Message['From'] ) ){
	$Receipt['To'] = $Message['From'];
}
$Receipt += array (
	'Date' => date( 'c', $Timestamp ),
	'Message-Type' => 'receipt',
	'Original-Message' => $_POST['message']
);


// Process message

include $messageHandler;


// Deliver receipt (if message handler has not exited the script)

$Receipt['Elapsed-Time'] = sprintf( '%.3f ms', 1000 * ( microtime( TRUE ) - $startTime ) );
$Receipt = spyc_dump( $Receipt );
$signature = generate_signature( $Receipt );
if ( ! $signature ){
	http_error( '500 Internal Server Error', "Could not sign message" );
}
$Receipt = "Signature: {$signature}\n{$Receipt}";
if ( ! empty( $SaveReceiptAs ) ){
	foreach ( (array) $SaveReceiptAs as $file ){
		put_file( $file, $Receipt );
	}
}
echo $Receipt;


// end of file process.php

