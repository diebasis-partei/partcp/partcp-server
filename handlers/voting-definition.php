<?php

// Handler for 'voting-definition' messages

if ( empty( $Config['Registrars'] ) ){
	http_error( '500 Internal Server Error', 'No registrar configured' );
}

if ( ! get_sender_key() || ! $senderIsRegistrar ){
	http_error( '403 Forbidden', 'Sender is not a registrar' );
}

if ( empty( $Message['Signature'] )
	|| empty( $Message['From'] )
	|| empty( $Message['Voting-Name'] )
	|| empty( $Message['Voting-Title'] )
	|| empty( $Message['Voting-Type'] )
	|| empty( $Message['Voting-Options'] )
){
	http_error( '400 Bad Request', 'Incomplete message' );
}

if ( ! verify_signature() ){
	http_error( '403 Forbidden', 'Signature is not valid' );
}

$id = strtolower( $Message['Voting-Name'] );
$votingModel = new Votings( $DataDir );
if ( ! empty( $Message['Event-Id'] ) ){
	$events = new Events( $DataDir );
	$votingModel->set_base_dir( $events->get_dir( $Message['Event-Id'] ) );
}
$dir = $votingModel->get_dir( $id );
$success = make_directory( $dir );
if ( ! $success ){
	http_error( '500 Internal Server Error', 'Could not create voting directory' );
}

$SaveReceiptAs = "{$dir}/voting-definition";
$Receipt['Voting-Id'] = $id;


// end of file handlers/voting-definition.php

