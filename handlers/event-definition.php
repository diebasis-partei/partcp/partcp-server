<?php

// Handler for 'event-definition' messages

if ( empty( $Config['Registrars'] ) ){
	http_error( '500 Internal Server Error', 'No registrar configured' );
}

if ( ! get_sender_key() || ! $senderIsRegistrar ){
	http_error( '403 Forbidden', 'Sender is not a registrar' );
}

if ( empty( $Message['Signature'] )
	|| empty( $Message['From'] )
){
	http_error( '400 Bad Request', 'Incomplete message' );
}

if ( empty( $Message['Event-Date'] ) ){
	$date = time();
}
else {
	$date = strtotime( $Message['Event-Date'] );
	if ( ! $date || date( 'Ymd', $date ) < date('Ymd') ){
		http_error( '400 Bad Request', 'Invalid event date' );
	}
}

if ( ! verify_signature() ){
	http_error( '403 Forbidden', 'Signature is not valid' );
}

if ( empty( $Message['Event-Name'] ) ){
	$name = 'event-' . substr( strrchr( microtime( TRUE ), '.' ), 1 );
}
else {
	$name = substr( str2filename( $Message['Event-Name'] ), 0, 32 );
}

$events = new Events( $DataDir );
$id = date( 'Ymd', $date ) . "-{$name}";
$dir = $events->get_dir( $id );
$success = make_directory( $dir );
if ( ! $success ){
	http_error( '500 Internal Server Error', 'Could not create event directory' );
}

$fileName = date( 'Ymd-His', $Timestamp ) . '-event-definition';
$SaveReceiptAs = "{$dir}/{$fileName}";
$Receipt['Event-Id'] = $id;


// end of file handlers/event-definition.php

