<?php

// Handler for "ping" messages

if ( ! empty( $Message['Signature'] ) ){
	if ( ! empty( $Message['From'] ) ){
		$pubKey = get_sender_key();
		if ( ! $pubKey ){
			$Receipt['Verification-Result'] = 'unknown sender';
		}
		elseif ( verify_signature() ){
			$Receipt['Verification-Result'] = 'valid signature';
		}
		else {
			$Receipt['Verification-Result'] = 'invalid signature';
		}
	}
	elseif ( ! empty( $Message['Public-Key'] ) ){
		if ( verify_signature( TRUE ) ){
			$Receipt['Verification-Result'] = 'valid signature (unknown sender)';
		}
		else {
			$Receipt['Verification-Result'] = 'invalid signature (unknown sender)';
		}
	}
	else {
		$Receipt['Verification-Result'] = 'signature could not be verified';
	}
}
else {
	$Receipt['Verification-Result'] = 'missing signature, nothing to verify';
}

foreach ( $Message as $key => $value ){
	if ( substr( $key, -1 ) == '~' ){
		$key = substr( $key, 0, -1 );
		$Receipt[ "Decrypt-{$key}" ] = $Message[ $key ] ?? 'NOT DECRYPTABLE';
	}
	elseif ( substr( $key, 0, 8 ) == 'Encrypt-' ){
		$Receipt[ substr( $key, 8 ) . '~' ] = encrypt_value( $value );
	}
}

$publicKey = Partcp::get_local_pubkey();

if ( $publicKey ){
	$Receipt['Public-Key'] = base64_encode( $publicKey );
}


// end of file handlers/ping.php

