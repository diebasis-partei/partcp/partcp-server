<?php

// Handler for 'lot-invalidation' messages

if ( empty( $Config['Registrars'] ) ){
	http_error( '500 Internal Server Error', 'No registrar configured' );
}

if ( ! get_sender_key() || ! $senderIsRegistrar ){
	http_error( '403 Forbidden', 'Sender is not a registrar' );
}

if ( empty( $Message['Lot-Code'] ) && empty( $Message['Lot-Codes'] ) ){
	http_error( '400 Bad Request', 'Incomplete message' );
}

if ( ! verify_signature() ){
	http_error( '403 Forbidden', 'Signature is not valid' );
}

if ( ! empty( $Message['Event-Id'] ) ){
	$events = new Events( $DataDir );
	$eventDir = $events->get_dir( $Message['Event-Id'] );
}
else {
	$eventDir = '';
}

// Retrieve lots contents
$lotsModel = new Lots( $DataDir, $Config['Lots-Model'] );
$lotsModel->set_base_dir( $eventDir );
$lotCodes = array_merge( (array) ( $Message['Lot-Code'] ?? [] ), (array) ( $Message['Lot-Codes'] ?? [] ) );
$ptcpIds = array ();
foreach ( $lotCodes as $lotCode ){
	$lotName = generate_hash( $lotCode );
	$lotDir = $lotsModel->get_dir( $lotName, TRUE );
	if ( ! file_exists( $lotDir ) ){
		http_error( '404 Not Found', 'Unknown lot code' );
	}
	$lotContent = file_get_contents( "{$lotDir}/lot" );
	if ( empty( $lotContent ) ){
		http_error( '500 Internal Server Error', 'Lot could not be read' );
	}
	$lotContent = decrypt_local( $lotContent );
	if ( empty( $lotContent ) ){
		http_error( '500 Internal Server Error', 'Lot content could not be decrypted' );
	}
	$lotContent = spyc_load( $lotContent );
	if ( empty( $lotContent['participant_id'] ) ){
		http_error( '500 Internal Server Error', 'Invalid lot content' );
	}
	$SaveReceiptAs[] = "{$lotDir}/lot-invalidation";
	$ptcpIds[] = $lotContent['participant_id'];
}

// Invalidate participants
$ptcpModel = new Participants( $DataDir, $Config['Participants-Model'] );
$ptcpModel->set_base_dir( $eventDir );
foreach ( $ptcpIds as $ptcpId ){
	$ptcpDir = $ptcpModel->get_dir( $ptcpId, TRUE );
	if ( file_exists( $ptcpDir ) ){
		$fileName = date( 'Ymd-His', $Timestamp ) . '-lot-invalidation';
		$Receipt['Participant-Directories'][] = $ptcpDir;
		$SaveReceiptAs[] = "{$ptcpDir}/{$fileName}";
	}
}


// end of file handlers/lot-invalidation.php

