NAME
----
ballot

DESCRIPTION
-----------
A ballot containing votes of a voting participant.

ARGUMENTS
---------
- Voting-Id
- Votes

NOTES
-----
The value of the Votes argument is a list of vote objects
with `id` and `vote` attributes.

The message must be signed by a valid voting participant.

