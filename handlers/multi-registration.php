<?php

// Handler for 'multi-registration' messages


function generate_code( $args = [] ){
	$charList = (string) ( $args['char_list'] ?? 'ABCDEFGHJKLMNPQRSTUVWXYZ123456789' );
	$finalLength = (int) ( $args['final_length'] ?? 16 );
	$groupLength = $args['group_length'] ?? 4;
	$groupSeparator = $args['group_separator'] ?? '-'; 
	$crcLength = $args['crc_length'] ?? 0;
	$netLength = $finalLength - $crcLength;
	$charListLength = strlen( $charList ) - 1;
	$code = '';
	for ( $x = 1; $x <= $netLength; $x++ ){
		$code .= $charList[ rand( 0, $charListLength ) ];
	}
	if ( $groupLength ){
		$code .= str_repeat( ' ', $crcLength );
		$code = substr( chunk_split( $code, $groupLength, $groupSeparator ), 0, -1 );
		$code = rtrim( $code, ' ' );
	}
	if ( $crcLength ){
		$code .= substr( crc32( $code ), - $crcLength );
	}
	elseif ( $groupLength ){
		$code = rtrim( $code, $groupSeparator );
	}
	return $code;
}

if ( empty( $Config['Registrars'] ) ){
	http_error( '500 Internal Server Error', 'No registrar configured' );
}

if ( ! get_sender_key() || ! $senderIsRegistrar ){
	http_error( '403 Forbidden', 'Sender is not a registrar' );
}

if ( empty( $Message['Signature'] )
	|| empty( $Message['From'] )
	|| empty( $Message['Count'] )
){
	http_error( '400 Bad Request', 'Incomplete registration message' );
}

if ( ! verify_signature() ){
	http_error( '403 Forbidden', 'Signature is not valid' );
}

$count = (int) $Message['Count'];
if ( $count < 1 || $count > 10000 ){
	http_error( '400 Bad Request', 'Invalid value for Count (1..10000)' );
}

$rules = array (
	'prefix' => 'p',
	'counter_start' => 1,
	'group_length' => 0,
	'group_separator' => '-', 
	'crc_length' => 0
);
if ( ! empty( $Message['Naming-Rules'] ) ){
	$rules = array_merge( $rules, $Message['Naming-Rules'] );
}
if ( empty( $rules['counter_width'] ) || $rules['counter_width'] < strlen( $count ) ){
	$rules['counter_width'] = strlen( $count );
}

$eventDir = '';
if ( ! empty( $Message['Event-Id'] ) ){
	$events = new Events( $DataDir );
	$eventDir = $events->get_dir( $Message['Event-Id'] );
}

$ptcp = new Participants( $DataDir, $Config['Participants-Model'] ?? [] );
$ptcp->set_base_dir( $eventDir );

if ( ! empty( $Message['Anonymization'] ) ){
	$lots = new Lots( $DataDir, $Config['Lots-Model'] ?? [] );
	$lots->set_base_dir( $eventDir );
}

set_time_limit( 30 + $count );
$counterEnd = $rules['counter_start'] + $count;
for ( $i = $rules['counter_start']; $i < $counterEnd; $i++ ){

	// generate id and create directory
	$id = $rules['prefix'] . sprintf( '%0' . $rules['counter_width'] . 'd', $i );
	if ( $rules['group_length'] ){
		$id .= str_repeat( ' ', $rules['crc_length'] );
		$id = substr( chunk_split( $id, $rules['group_length'], $rules['group_separator'] ), 0, -1 );
		$id = rtrim( $id, ' ' );
	}
	if ( $rules['crc_length'] ){
		$id .= substr( crc32( $id ), - $rules['crc_length'] );
	}
	$dir = $ptcp->get_dir( $id );
	if ( ! make_directory( $dir ) ){
		http_error( '500 Internal Server Error', "Could not create participant directory" );
	}

	// generate credential
	$credential = generate_code( $Message['Credential-Rules'] ?? [] );

	// create and save individual registration message
	$regMessage = array (
		'From' => $Config['Server-Name'],
		'Date' => date( 'c', $Timestamp ),
		'Message-Type' => 'registration',
		'Participant-Id' => $id,
		'Credential' => hash( 'sha256', $credential )
	);
	if ( ! empty( $Message['Division'] ) ){
		$regMessage['Division'] = $Message['Division'];
	}
	$regMessage['Original-Message'] = $_POST['message'];
	$regMessage = spyc_dump( $regMessage );
	$envMessage = array (
		'From' => $Config['Server-Name'],
		'Date' => date( 'c', $Timestamp ),
		'Message-Type' => 'receipt',
		'Original-Message' => $regMessage
	);
	$envMessage = spyc_dump( $envMessage );
	$signature = generate_signature( $envMessage );
	if ( ! $signature ){
		http_error( '500 Internal Server Error', "Could not sign message" );
	}
	$envMessage = "Signature: {$signature}\n{$envMessage}";
	$file = $dir . '/' . date( 'Ymd-His', $Timestamp ) . '-registration';
	put_file( $file, $envMessage );
	$participants[] = compact( 'id', 'credential' );

	// create lot, if anonymization requested
	if ( ! empty( $Message['Anonymization'] ) ){
		do {
			$lotCode = generate_code( $Message['Lot-Code-Rules'] ?? [] );
			$lotName = generate_hash( $lotCode );
			$lotDir = $lots->get_dir( $lotName );
		} while ( file_exists( "{$DataDir}/{$lotDir}" ) );
		$lotContent = "participant_id: {$id}\n"
			. "credential: {$credential}";
		put_file( "{$lotDir}/lot", encrypt_local( $lotContent ), TRUE );
		$lotCodes[] = $lotCode;
	}
}

// set response values
$Receipt['Message-Type'] = 'multi-registration-confirmation';
if ( ! empty( $Message['Anonymization'] ) ){
	$lotCodes = spyc_dump( $lotCodes );
	$Receipt['Lot-Codes~'] = encrypt_value( $lotCodes );
}
else {
	$participants = spyc_dump( $participants );
	$Receipt['Participants~'] = encrypt_value( $participants );
}


// end of file handlers/multi-registration.php

