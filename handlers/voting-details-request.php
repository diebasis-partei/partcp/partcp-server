<?php

// Handler for voting-details-request messages

if ( empty( $Message['Voting-Id'] ) ){
	http_error( '400 Bad Request', 'Incomplete message' );
}

$votingModel = new Votings( $DataDir );
if ( ! empty( $Message['Event-Id'] ) ){
	$events = new Events( $DataDir );
	$votingModel->set_base_dir( $events->get_dir( $Message['Event-Id'] ) );
}
$Receipt['Message-Type'] = 'voting-details';
$Receipt['Voting-Data'] = $votingModel->get_data( $Message['Voting-Id'] );


// end of file handlers/voting-details-request.php

