<?php

// Handler for 'ballot' messages

if ( empty( $Message['Signature'] )
	|| empty( $Message['From'] )
	|| empty( $Message['Voting-Id'] )
	|| empty( $Message['Votes'] )
){
	http_error( '400 Bad Request', 'Incomplete registration message' );
}

if ( ! empty( $Message['Event-Id'] ) ){
	$events = new Events( $DataDir );
	$eventsDir = $events->get_dir( $Message['Event-Id'] );
}

$votingModel = new Votings( $DataDir );
$votingModel->set_base_dir( $eventsDir ?? NULL );
$votingId = strtolower( $Message['Voting-Id'] );
$votingDir = $votingModel->get_dir( $votingId, TRUE );

if ( ! file_exists( "{$votingDir}/voting-start-declaration" ) ){
	http_error( '403 Forbidden', 'Voting has not been started yet' );
}

if ( file_exists( "{$votingDir}/voting-end-declaration" ) ){
	http_error( '403 Forbidden', 'Voting has been closed' );
}

if ( ! verify_signature() ){
	http_error( '403 Forbidden', 'Signature is not valid' );
}

if ( empty( $Sender ) ){
	http_error( '403 Forbidden', 'Unknown sender' );
}

$voting = $votingModel->get_data( $votingId );
if ( ! $voting ){
	http_error( '404 Not Found', 'Unknown voting ' . $votingId );
}
if ( ! empty( $voting['division'] ) ){
	if ( empty( $Sender['division'] ) 
		|| strpos( $Sender['division'], $voting['division'] ) !== 0 
	){
		http_error( '403 Forbidden', 'Sender is not entitled to participate in this voting' );
	}
}

list ( $senderId, $server ) = explode( '@', $Message['From'] ) + [ '', '' ];
if ( strpos( $senderId, '+' ) ){
	list( $eventId, $senderId ) = explode( '+', $senderId );
}

if ( empty( $Config['Participants-Model'] ) ){
	$senderDir = '/' .$senderId;
}
else {
	$ptcpModel = new Participants( $DataDir, $Config['Participants-Model'] );
	$senderDir = strstr( $ptcpModel->get_dir( $senderId ), '/' );
}
$ballotsDir = "{$votingDir}/ballots{$senderDir}";
$ballotFiles = glob( $ballotsDir . '/ballot-*' );
$nextNumber = $ballotFiles ? count( $ballotFiles ) + 1 : 1;
$fileName = sprintf( 'ballot-%04d', $nextNumber );

unset( $Receipt['Date'] );
$receipt = spyc_dump( $Receipt );
$signature = generate_signature( $receipt );
$receipt = "Signature: {$signature}\n{$receipt}";
put_file( "{$ballotsDir}/{$fileName}", $receipt, TRUE );

unset( $Receipt['Original-Message'] );


// end of file handlers/ballot.php

