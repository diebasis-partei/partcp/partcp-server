<?php

// Handler for help-request messages

unset( $Receipt['Original-Message'] );

if ( empty( $Message['Help-Topic'] ) ){
	$types = array ();
	$topics = array ();
	$dirScan = scandir( __DIR__ );
	foreach ( $dirScan as $item ){
		$info = pathinfo( $item );
		if ( empty( $info['extension'] ) ){
			continue;
		}
		if ( $info['extension'] == 'php' ){
			$types[] = $info['filename'];
		}
		if ( $info['extension'] == 'txt' ){
			$topics[] = $info['filename'];
		}
	}
	$Receipt['Message-Type'] = 'help';
	$Receipt['Help-Content'] = "Supported message types:\n\n" 
		. implode( ', ', $types ) . "\n\n"
		. "You may get additional help by sending a 'help-request' message\n"
		. "and providing a message type in the 'Help-Topic' field.";
}
else {
	$file =  __DIR__ . "/{$Message['Help-Topic']}.txt";
	$Receipt['Help-Topic'] = $Message['Help-Topic'];
	if ( file_exists( $file ) ){
		$Receipt['Help-Content'] = file_get_contents( $file );
	}
	else {
		$Receipt['Help-Content'] = 'Sorry, no help here for that topic.';
	}
}

// end of file handlers/help-request.php

