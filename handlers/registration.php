<?php

// Handler for 'registration' messages

if ( empty( $Config['Registrars'] ) ){
	http_error( '500 Internal Server Error', 'No registrar configured' );
}

if ( ! get_sender_key() || ! $senderIsRegistrar ){
	http_error( '403 Forbidden', 'Sender is not a registrar' );
}

if ( empty( $Message['Signature'] )
	|| empty( $Message['From'] )
	|| empty( $Message['Name'] )
	|| empty( $Message['Credential'] )
){
	http_error( '400 Bad Request', 'Incomplete registration message' );
}

if ( ! verify_signature() ){
	http_error( '403 Forbidden', 'Signature is not valid' );
}

$ptcp = new Participants( $DataDir, $Config['Participants-Model'] );

do {
	$code = rand( 10000, 99999 );
	$id = "{$Message['Name']}.{$code}";
	$dir = $ptcp->get_dir( $id );
	$success = make_directory( $dir );
}
while ( ! $success );

$fileName = date( 'Ymd-His', $Timestamp ) . '-registration';
$Receipt['Message-Type'] = 'registration-confirmation';
$Receipt['Participant-Id'] = $id;
$SaveReceiptAs = "{$dir}/{$fileName}";


// end of file handlers/registration.php

