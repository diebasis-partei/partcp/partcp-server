<?php

// Handler for 'key-renewal-permission' messages

if ( empty( $Config['Registrars'] ) ){
	http_error( '500 Internal Server Error', 'No registrar configured' );
}

if ( ! get_sender_key() || ! $senderIsRegistrar ){
	http_error( '403 Forbidden', 'Sender is not a registrar' );
}

if ( empty( $Message['Signature'] )
	|| empty( $Message['From'] )
	|| empty( $Message['Participant-Id'] )
	|| empty( $Message['Credential'] )
){
	http_error( '400 Bad Request', 'Incomplete key-renewal-permission message' );
}

if ( ! verify_signature() ){
	http_error( '403 Forbidden', 'Signature is not valid' );
}

$ptcp = new Participants( $DataDir, $Config['Participants-Model'] );

if ( ! empty( $Message['Event-Id'] ) ){
	$events = new Events( $DataDir );
	$ptcp->set_base_dir( $events->get_dir( $Message['Event-Id'] ) );
}

$dir = $ptcp->get_dir( $Message['Participant-Id'] );

if ( ! $dir ){
	http_error( '404 Not Found', 'Unknown participant ' . $id );
}

$fileName = date( 'Ymd-His', $Timestamp ) . '-key-renewal-permission';
$SaveReceiptAs = "{$dir}/{$fileName}";


// end of file handlers/key-renewal-permission.php

