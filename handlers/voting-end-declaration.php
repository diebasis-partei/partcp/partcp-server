<?php

// Handler for 'voting-end-declaration' messages

if ( empty( $Config['Registrars'] ) ){
	http_error( '500 Internal Server Error', 'No registrar configured' );
}

if ( ! get_sender_key() || ! $senderIsRegistrar ){
	http_error( '403 Forbidden', 'Sender is not a registrar' );
}

if ( empty( $Message['Signature'] )
	|| empty( $Message['From'] )
	|| empty( $Message['Voting-Id'] )
){
	http_error( '400 Bad Request', 'Incomplete message' );
}

if ( ! verify_signature() ){
	http_error( '403 Forbidden', 'Signature is not valid' );
}

$votingModel = new Votings( $DataDir );
if ( ! empty( $Message['Event-Id'] ) ){
	$events = new Events( $DataDir );
	$votingModel->set_base_dir( $events->get_dir( $Message['Event-Id'] ) );
}

$id = strtolower( $Message['Voting-Id'] );
$voting = $votingModel->get_data( $id );

if ( ! $voting ){
	http_error( '404 Not Found', 'Voting does not exist' );
}

if ( $voting['status'] != 'open' ){
	http_error( '500 Internal Server Error', 'Voting has not been started' );
}

$SaveReceiptAs = $votingModel->get_dir( $id, TRUE ) . '/voting-end-declaration';


// end of file handlers/voting-end-declaration.php

