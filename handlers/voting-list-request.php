<?php

// Handler for voting-list-request messages

$votingsModel = new Votings( $DataDir );
if ( ! empty( $Message['Event-Id'] ) ){
	$events = new Events( $DataDir );
	$votingsModel->set_base_dir( $events->get_dir( $Message['Event-Id'] ) );
}
$status = $Message['Voting-Status'] ?? 'open';
$votings = $votingsModel->get_list( $status );
$Receipt['Message-Type'] = 'voting-list';
$Receipt['Votings'] = $votings;

// end of file handlers/voting-list-request.php

