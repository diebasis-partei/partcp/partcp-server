<?php

// Handler for 'key-submission' messages

if ( empty( $Message['Signature'] )
	|| empty( $Message['From'] )
	|| empty( $Message['Public-Key'] )
	|| empty( $Message['Credential'] )
){
	http_error( '400 Bad Request', 'Incomplete key-submission message' );
}

list ( $id, $server ) = explode( '@', $Message['From'] ) + [ '', '' ];
if ( ! empty( $server ) && $server != $Config['Server-Name'] ){
	http_error( '400 Bad Request', 'Invalid sender domain' );
}

if ( ! verify_signature( TRUE ) ){
	http_error( '400 Bad Request', 'Signature is not valid' );
}

$ptcp = new Participants( $DataDir, $Config['Participants-Model'] );

if ( ! empty( $Message['Event-Id'] ) ){
	$events = new Events( $DataDir );
	$ptcp->set_base_dir( $events->get_dir( $Message['Event-Id'] ) );
}

$data = $ptcp->get_data( $id );

if ( ! $data ){
	http_error( '404 Not Found', 'Unknown sender ' . $id );
}

if ( empty( $data['credential'] ) 
	|| $data['credential'] != hash( 'sha256', $Message['Credential'] )
){
	http_error( '403 Forbidden', 'Wrong credential or key submission not permitted' );
}

$fileName = date( 'Ymd-His', $Timestamp ) . '-key-submission';
$dir = $ptcp->get_dir( $id );
$SaveReceiptAs = "{$dir}/{$fileName}";


// end of file handlers/key-submission.php

