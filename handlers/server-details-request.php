<?php

// Handler for server-details-request messages

$Receipt['Message-Type'] = 'server-details';
$Receipt['Server-Data'] = array (
	'name' => $Config['Server-Name'] ?? NULL,
	'community' => $Config['Community-Name'] ?? NULL,
	'participant_id_format' => $Config['Participant-Id-Format'] ?? NULL,
	'participant_credential_format' => $Config['Participant-Credential-Format'] ?? NULL,
	'public_key' => base64_encode( Partcp::get_local_pubkey() )
);
$eventsModel = new Events( $DataDir );
$Receipt['Events'] = $eventsModel->get_list();

// end of file handlers/server-details-request.php

