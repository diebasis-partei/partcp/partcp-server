<?php

// Handler for 'lot-request' messages

if ( empty( $Message['Lot-Code'] )
	|| empty( $Message['Public-Key'] )
){
	http_error( '400 Bad Request', 'Incomplete lot-request message' );
}

if ( ! verify_signature( TRUE ) ){
	http_error( '403 Forbidden', 'Signature is not valid' );
}

if ( empty( $Message['Event-Id'] ) ){
	$dirPrefix = '';
}
else {
	$events = new Events( $DataDir );
	$dirPrefix = $events->get_dir( $Message['Event-Id'] ) . '/';
}

$lots = new Lots( $DataDir, $Config['Lots-Model'] );
$lotName = generate_hash( $Message['Lot-Code'] );
$lotDir = "{$DataDir}/{$dirPrefix}" . $lots->get_dir( $lotName );
if ( ! file_exists( $lotDir ) ){
	http_error( '404 Not Found', 'Unknown lot code' );
}
if ( file_exists( "{$lotDir}/lot-invalidation" ) ){
	http_error( '410 Gone', 'Lot has been invalidated' );
}
if ( file_exists( "{$lotDir}/pick-up-note" ) ){
	http_error( '410 Gone', 'Lot has been picked up already' );
}
$lotContent = file_get_contents( "{$lotDir}/lot" );
if ( empty( $lotContent ) ){
	http_error( '500 Internal Server Error', 'Lot could not be read' );
}
$lotContent = decrypt_local( $lotContent );
if ( empty( $lotContent ) ){
	http_error( '500 Internal Server Error', 'Lot content could not be decrypted' );
}
$msg['Message-Type'] = 'pick-up-note';
$msg['Lot-Code'] = $Message['Lot-Code'];
$result = put_file( "{$lotDir}/pick-up-note", spyc_dump( $msg ), TRUE );
if ( ! $result ){
	http_error( '500 Internal Server Error', 'Could not create pick-up note' );
}

$Receipt['Message-Type'] = 'lot';
$Receipt['Error'] = Partcp::$lastError;
$Receipt['Lot-Content~'] = encrypt_value( $lotContent );


// end of file handlers/lot-request.php

