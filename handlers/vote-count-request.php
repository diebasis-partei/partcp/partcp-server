<?php

// Handler for vote-count-request messages

if ( empty( $Config['Registrars'] ) ){
	http_error( '500 Internal Server Error', 'No registrar configured' );
}

if ( ! get_sender_key() || ! $senderIsRegistrar ){
	http_error( '403 Forbidden', 'Sender is not a registrar' );
}

if ( empty( $Message['Signature'] )
	|| empty( $Message['From'] )
	|| empty( $Message['Voting-Id'] )
){
	http_error( '400 Bad Request', 'Incomplete message' );
}

if ( ! verify_signature() ){
	http_error( '403 Forbidden', 'Signature is not valid' );
}

$votingModel = new Votings( $DataDir );
if ( ! empty( $Message['Event-Id'] ) ){
	$events = new Events( $DataDir );
	$votingModel->set_base_dir( $events->get_dir( $Message['Event-Id'] ) );
}
$voting = $votingModel->get_data( $Message['Voting-Id'] );
if ( ! $voting ){
	http_error( '404 Not Found', 'Voting does not exist' );
}
if ( $voting['status'] == 'finished' ){
	http_error( '403 Forbidden', 'Votes have already been counted' );
}
if ( $voting['status'] != 'closed' ){
	http_error( '403 Forbidden', 'Voting has not ended yet' );
}
$dir = $votingModel->get_dir( $Message['Voting-Id'] );
$result = $votingModel->count_votes( $Message['Voting-Id'] );
$Receipt['Message-Type'] = 'vote-count';
$Receipt['Voting-Result'] = $result;
if ( ! file_exists( "{$dir}/vote-count" ) ){
	$SaveReceiptAs = "{$dir}/vote-count";
}

// end of file handlers/vote-count-request.php

