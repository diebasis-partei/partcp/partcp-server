<?php

class Partcp {

	static $localPubkey;
	static $lastError;
	static $socket;
	static $serverSocket = '/etc/partcp/socket';
	static $clientSocket;
	static $verbosity = FALSE;

	static function init( $clientSocket ){
		self::$lastError = '';
		if ( ! empty( self::$socket ) ){
			return TRUE;
		}
		self::$clientSocket = $clientSocket;
		self::$socket = socket_create( AF_UNIX, SOCK_DGRAM, 0 );
		if ( empty( self::$socket ) ){
			self::$lastError = 'Unable to create AF_UNIX socket';
			return FALSE;
		}
		if ( ! socket_bind( self::$socket, $clientSocket ) ){
			self::$lastError = "Unable to bind to {$clientSocket}";
			return FALSE;
		}
		return TRUE;
	}


	static function shutdown(){
		if ( ! empty( self::$socket ) ){
			socket_close( self::$socket );
		}
		if ( ! empty( self::$clientSocket ) ){
			unlink( self::$clientSocket );
		}
	}


	static function get_local_pubkey(){
		if ( empty( self::$localPubkey ) ){
			self::$localPubkey = file_get_contents('/etc/partcp/public_key');
		}
		return self::$localPubkey;
	}


	static function sign( $data ){
		self::$lastError = '';
		self::send_to_server('SIGN');
		$response = self::receive_from_server();
		if ( $response != 'DATA?' ){
			self::$lastError = 'Partcpd does not respond as expected';
			return FALSE;
		}
		self::send_stream_to_server( $data );
		$signature = self::receive_from_server();
		return $signature;
	}


	static function verify( $signature, $data, $pubKey = NULL ){
		self::$lastError = '';
		if ( $pubKey ){
			if ( strlen( $signature ) != SODIUM_CRYPTO_SIGN_BYTES ){
				self::$lastError = 'Invalid signature string';
				return FALSE;
			}
			if ( strlen( $pubKey ) != SODIUM_CRYPTO_SIGN_PUBLICKEYBYTES ){
				self::$lastError = 'Invalid public key string';
				return FALSE;
			}
			$result = sodium_crypto_sign_verify_detached( $signature, $data, $pubKey );
			return $result == 1;
		}
		// No pubKey given, so use partcpd to verify
		self::send_to_server('VERIFY');
		$response = self::receive_from_server();
		if ( $response != 'DATA?' ){
			self::$lastError = 'Partcpd does not respond as expected';
			return FALSE;
		}
		self::send_stream_to_server( $data );
		$response = self::receive_from_server();
		if ( $response != 'SIGNATURE?' ){
			self::$lastError = 'Partcpd does not respond as expected';
			return FALSE;
		}
		self::send_to_server( $signature );
		$response = self::receive_from_server();
		return $response == '1';
	}


	static function hash( $data ){
		self::$lastError = '';
		self::send_to_server('HASH');
		$response = self::receive_from_server();
		if ( $response != 'DATA?' ){
			self::$lastError = 'Partcpd does not respond as expected';
			return FALSE;
		}
		self::send_stream_to_server( $data );
		$hashCode = self::receive_from_server();
		return $hashCode;
	}


	static function encrypt( $data, $pubKey = NULL ){
		self::$lastError = '';
		$pubKey = $pubKey ?? substr( self::get_local_pubkey(), SODIUM_CRYPTO_SIGN_PUBLICKEYBYTES );
		if ( ! $pubKey || strlen( $pubKey ) != SODIUM_CRYPTO_BOX_PUBLICKEYBYTES ){
			self::$lastError = 'Invalid public key string';
			return FALSE;
		}
		self::send_to_server('ENCRYPT');
		$response = self::receive_from_server();
		if ( $response != 'PUBLIC KEY?' ){
			self::$lastError = 'Partcpd does not respond as expected';
			return FALSE;
		}
		self::send_to_server( $pubKey );
		$response = self::receive_from_server();
		if ( $response != 'DATA?' ){
			self::$lastError = 'Partcpd does not respond as expected';
			return FALSE;
		}
		self::send_stream_to_server( $data );
		$encrypted = self::receive_stream_from_server();
		return $encrypted;
	}


	static function decrypt( $data, $pubKey = NULL ){
		self::$lastError = '';
		$pubKey = $pubKey ?? substr( self::get_local_pubkey(), SODIUM_CRYPTO_SIGN_PUBLICKEYBYTES );
		if ( ! $pubKey || strlen( $pubKey ) != SODIUM_CRYPTO_BOX_PUBLICKEYBYTES ){
			self::$lastError = 'Invalid public key string';
			return FALSE;
		}
		self::send_to_server('DECRYPT');
		$response = self::receive_from_server();
		if ( $response != 'PUBLIC KEY?' ){
			self::$lastError = 'Partcpd does not respond as expected';
			return FALSE;
		}
		self::send_to_server( $pubKey );
		$response = self::receive_from_server();
		if ( $response != 'DATA?' ){
			self::$lastError = 'Partcpd does not respond as expected';
			return FALSE;
		}
		self::send_stream_to_server( $data );
		$decrypted = self::receive_stream_from_server();
		return $decrypted;
	}


	static function mkdir( $path, $zeroTime = FALSE ){
		self::$lastError = '';
		self::send_to_server( 'MKDIR' . ( $zeroTime ? '0' : '' ));
		$response = self::receive_from_server();
		if ( $response != 'PATH?' ){
			self::$lastError = 'Partcpd does not respond as expected';
			return FALSE;
		}
		self::send_to_server( $path );
		$response = self::receive_from_server();
		return $response == 'OK';
	}


	static function put( $path, $data, $zeroTime = FALSE ){
		self::$lastError = '';
		self::send_to_server( 'PUT' . ( $zeroTime ? '0' : '' ));
		$response = self::receive_from_server();
		if ( $response != 'PATH?' ){
			self::$lastError = 'Partcpd does not respond as expected';
			return FALSE;
		}
		self::send_to_server( $path );
		$response = self::receive_from_server();
		if ( $response != 'DATA?' ){
			self::$lastError = 'Partcpd does not respond as expected';
			return FALSE;
		}
		self::send_stream_to_server( $data );
		$response = self::receive_from_server();
		return $response == 'OK';
	}


	static function get_port_number(){
		static $portNumber;
		if ( empty( $portNumber ) ){
			$portFile = '/etc/partcp/port';
			$portNumber = file_get_contents( $portFile );
		}
		return $portNumber;
	}


	static function receive_from_server( $timeOut = 5 ){
		if ( ! socket_set_block( self::$socket ) ){
			echo "ERROR: Unable to set blocking mode for socket\n";
			return FALSE;
		}
		socket_set_option( self::$socket, SOL_SOCKET, SO_RCVTIMEO, [ 'sec' => $timeOut, 'usec' => 0 ] );
		$bytesReceived = socket_recvfrom( self::$socket, $data, 65536, 0, $from );
		if ( $bytesReceived === FALSE ){
			echo "ERROR: Could not receive data from socket\n";
			return FALSE;
		}
		if ( self::$verbosity ){
			$str = str_replace( "\n", ' ', substr( $data, 0, 70 ) );
			$str .= $data > 70 ? '...' : '';
			echo "<- ($bytesReceived) $str\n";
		}
		return $data;
	}


	static function send_to_server( $data ){
		if ( ! socket_set_nonblock( self::$socket ) ){
			echo "ERROR: Unable to set nonblocking mode for socket\n";
			return FALSE;
		}
		$length = strlen( $data );
		$bytesSent = socket_sendto( self::$socket, $data, $length, 0, self::$serverSocket );
		if ( $bytesSent === FALSE ){
			echo "ERROR: Could not send {$length} B to {self::$serverSocket}\n";
			return FALSE;
		}
		if ( $bytesSent != $length ){
			echo "ERROR: Length mismatch - {$bytesSent} B sent, {$length} B expected\n";
			return FALSE;
		}
		if ( self::$verbosity ){
			$str = str_replace( "\n", ' ', substr( $data, 0, 70 ) );
			$str .= $data > 70 ? '...' : '';
			echo "-> ($bytesSent) $str\n";
		}
		return TRUE;
	}


	static function receive_stream_from_server( $timeOut = 5 ){
		$data = '';
		while ( TRUE ) {
			$chunk = self::receive_from_server();
			if ( ! $chunk ){
				break;
			}
			$data .= $chunk;
			$result = self::send_to_server('MORE?');
			if ( ! $result ){
				break;
			}
		}
		return $data;
	}


	static function send_stream_to_server( $data ){
		$chunks = str_split( $data, 65536 );
		foreach ( $chunks as $chunk ) {
			$result = self::send_to_server( $chunk );
			if ( ! $result ){
				return FALSE;
			}
			$answer = self::receive_from_server();
			if ( $answer != 'MORE?' ){
				return FALSE;
			}
		}
		self::send_to_server('');
		return TRUE;
	}


}

// end of file partcp.class.php

